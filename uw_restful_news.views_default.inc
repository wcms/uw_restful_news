<?php

/**
 * @file
 * uw_restful_news.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function uw_restful_news_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'pathes';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'URL alias';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'URL Alias';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'views_json';
  $handler->display->display_options['style_options']['plaintext_output'] = 1;
  $handler->display->display_options['style_options']['remove_newlines'] = 0;
  $handler->display->display_options['style_options']['jsonp_prefix'] = '';
  $handler->display->display_options['style_options']['using_views_api_mode'] = 0;
  $handler->display->display_options['style_options']['translate_labels'] = 0;
  $handler->display->display_options['style_options']['object_arrays'] = 0;
  $handler->display->display_options['style_options']['numeric_strings'] = 0;
  $handler->display->display_options['style_options']['bigint_string'] = 0;
  $handler->display->display_options['style_options']['pretty_print'] = 0;
  $handler->display->display_options['style_options']['unescaped_slashes'] = 0;
  $handler->display->display_options['style_options']['unescaped_unicode'] = 0;
  $handler->display->display_options['style_options']['char_encoding'] = array();
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published status */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Node: URL alias */
  $handler->display->display_options['filters']['alias']['id'] = 'alias';
  $handler->display->display_options['filters']['alias']['table'] = 'views_url_alias_node';
  $handler->display->display_options['filters']['alias']['field'] = 'alias';
  $handler->display->display_options['filters']['alias']['exposed'] = TRUE;
  $handler->display->display_options['filters']['alias']['expose']['operator_id'] = 'alias_op';
  $handler->display->display_options['filters']['alias']['expose']['label'] = 'URL alias';
  $handler->display->display_options['filters']['alias']['expose']['operator'] = 'alias_op';
  $handler->display->display_options['filters']['alias']['expose']['identifier'] = 'alias';
  $handler->display->display_options['filters']['alias']['expose']['remember_roles'] = array(
    2 => '2',
  );

  /* Display: URL Aliases */
  $handler = $view->new_display('page', 'URL Aliases', 'page');
  $handler->display->display_options['path'] = 'api/v1.0/alias';
  $translatables['pathes'] = array(
    t('Master'),
    t('URL Alias'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('Nid'),
    t('URL alias'),
    t('URL Aliases'),
  );
  $export['pathes'] = $view;

  return $export;
}
