<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('News Type'),
  'resource' => 'news_type',
  'name' => 'news_type__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'story_type',
  'description' => t('Export the news type without metadata.'),
  'class' => 'RestfulNewsTypeResource',
);
