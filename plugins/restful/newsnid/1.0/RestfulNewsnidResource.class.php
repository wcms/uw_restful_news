<?php

/**
 * @file
 */

/**
 *
 */
class RestfulNewsnidResource extends RestfulEntityBaseNode {

  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {

    // The public fields for this endpoint.
    $public_fields = parent::publicFieldsInfo();
    
    $public_fields['id'] = array(
      'property' => 'nid',
      'process_callbacks' => array(
        array($this, 'getUwPath'),
      ),
    );

    // Return the list of public fields.
    return $public_fields;
  }

  /**
   * Get the path alias of a node.
   *
   * @param int $nid
   *   The entity id.
   *
   * @return string
   *   The path alias.
   */
  protected function getUwPath($nid): string {

   	$params = drupal_get_query_parameters();
   	
   	
   	
   	$path_to_node = url("node/" . $nid);

    // Return the path alias.
    return 	$path_to_node;
  }

}
