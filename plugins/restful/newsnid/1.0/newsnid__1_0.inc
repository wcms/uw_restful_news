<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('News nid'),
  'resource' => 'newsnid',
  'name' => 'newsnid__1_0',
  'entity_type' => 'node',
  'bundle' => 'uw_stories',
  'description' => t('A nid from UW news.'),
  'class' => 'RestfulNewsnidResource',
);
