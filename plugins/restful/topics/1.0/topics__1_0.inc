<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('Topics'),
  'resource' => 'topics',
  'name' => 'topics__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'uw_stories_societal_relevance',
  'description' => t('Export the topics without metadata.'),
  'class' => 'RestfulTopicsResource',
);
