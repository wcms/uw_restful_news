<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('News'),
  'resource' => 'news',
  'name' => 'news__1_0',
  'entity_type' => 'node',
  'bundle' => 'uw_stories',
  'description' => t('A node from UW news.'),
  'class' => 'RestfulNewsResource',
);
