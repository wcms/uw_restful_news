<?php
/**
 * @file
 * Extends the RestfulEntityBaseTaxonomyTerm class.
 */
class RestfulFacultiesResource extends RestfulEntityBaseTaxonomyTerm {

  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();
    return $public_fields;
  }

}
