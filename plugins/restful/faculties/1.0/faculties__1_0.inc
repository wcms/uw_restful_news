<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('Faculties'),
  'resource' => 'faculties',
  'name' => 'faculties__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'uw_stories_areas',
  'description' => t('Export the faculties without metadata.'),
  'class' => 'RestfulFacultiesResource',
);
