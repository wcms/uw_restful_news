<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('Audience'),
  'resource' => 'audience',
  'name' => 'audience__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'uwaterloo_audience',
  'description' => t('Export the audience without metadata.'),
  'class' => 'RestfulAudienceResource',
);
